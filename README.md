# forge

## Docker image layers

Layered approach towards an efficient docker cache optimization:

| Instruction | Name               | Customization                                                | Default              |
|-------------|--------------------|--------------------------------------------------------------|----------------------|
| FROM        | Dockerfile         | default base image                                           | Debian:bullseye-slim |
| ARG         | THEIA_NODE_CHANNEL | node channel to use for building and running theia           | 10                   |
| COPY        | package.json       | desired extensions, plugins, configurations for theia build  |                      |
| ARG         | install-theia.sh   |                                                              |                      |
| COPY        | run-as-root.sh     | install system packages                                      |                      |
| ARG         | USERNAME           | default image user                                           | craftsman            |
| ARG         | UID                |                                                              | 1000                 |
| ARG         | GID                |                                                              | 1000                 |
| ARG         | SHELL              |                                                              | /bin/bash            |
| COPY        | run-as-user.sh     | install custom packages that should not be installed as root |                      |
| COPY        | settings.json      | custom runtime settings for theia                            | {}                   |