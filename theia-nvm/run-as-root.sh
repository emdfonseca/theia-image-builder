#!/usr/bin/env sh

PACKAGES="git curl ca-certificates"

##
## install packages
##
apt-get update
apt-get install -y ${PACKAGES} --no-install-recommends
rm -rf /var/lib/apt/lists/*
