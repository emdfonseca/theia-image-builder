#!/usr/bin/env sh

[ $# -eq 1 ] || exit 1

channel=$1

build_dependencies="ca-certificates curl build-essential dirmngr gnupg python-minimal libpython-stdlib xz-utils"

apt-get update
apt-get install -y ${build_dependencies} --no-install-recommends
mkdir /tmp/dependencies
export PATH=/tmp/dependencies/bin:${PATH}

##
## install node
##
keys="
94AE36675C464D64BAFA68DD7434390BDBE9B9C5
FD3A5288F042B6850C66B31F09FE44734EB7990E
71DCFD284A79C3B38668286BC97EC7A07EDE3FC1
DD8F2338BAE7501E3DD5AC78C273792F7D83545D
C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8
B9AE9905FFD7803F25714661B63B535A4C206CA9
77984A986EBC2AA786BC0F66B01FBB92821C587A
8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600
4ED778F539E3634C779C87C6D7062848A1AB005C
A48C2BEE680E841632CD4E44F07496B3EB3C1762
B9E2F5981AA6E0CD28160D9FF13993A75599653C
"
root_url=https://nodejs.org/dist/latest-v${channel}.x
filename=$(curl -s ${root_url}/SHASUMS256.txt | grep -Po 'node-(.*?)-linux-x64.tar.xz')

for key in $keys; do
    gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" || \
    gpg --batch --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
    gpg --batch --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key";
done

curl -fsSLO --compressed ${root_url}/SHASUMS256.txt
curl -fsSLO --compressed ${root_url}/SHASUMS256.txt.sig
curl -fsSLO --compressed ${root_url}/${filename}

gpg --batch --verify SHASUMS256.txt.sig SHASUMS256.txt
grep " $filename\$" SHASUMS256.txt | sha256sum -c -
tar -xJf ${filename} -C /tmp/dependencies --strip-components=1 --no-same-owner
rm SHASUMS256.txt SHASUMS256.txt.sig ${filename}

##
## install yarn
##
curl -fSLO --compressed https://yarnpkg.com/latest.tar.gz
curl -fSLO --compressed https://yarnpkg.com/latest.tar.gz.asc
curl -L https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --import
gpg --verify latest.tar.gz.asc
tar -xzf latest.tar.gz -C /tmp/dependencies --strip-components=1 --no-same-owner
rm latest.tar.gz.asc latest.tar.gz

##
## install theia
##
yarn --pure-lockfile
NODE_OPTIONS="--max_old_space_size=4096" yarn theia build
yarn --production
yarn autoclean --init
echo *.ts >> .yarnclean
echo *.ts.map >> .yarnclean
echo *.spec.* >> .yarnclean
yarn autoclean --force
yarn cache clean

##
## delete all unnecessary apt packages
##
apt-mark auto '.*' > /dev/null
find /usr/local -type f -executable -exec ldd '{}' ';' \
      | awk '/=>/ { print $(NF-1) }' \
      | sort -u \
      | xargs -r dpkg-query --search \
      | cut -d: -f1 \
      | sort -u \
      | xargs -r apt-mark manual
npx modclean -r -n default:safe
mv /tmp/dependencies/bin/node /usr/local/share/theia/
rm -rf /tmp/dependencies
apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false
rm -rf /var/lib/apt/lists/*
