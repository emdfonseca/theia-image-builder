#!/usr/bin/env sh

PACKAGES="git neovim curl ca-certificates"

##
## install packages
##
apt-get update
apt-get install -y ${PACKAGES} --no-install-recommends

##
## install go
##
curl -fSLO --compressed https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz
echo 6272d6e940ecb71ea5636ddb5fab3933e087c1356173c61f4a803895e947ebb3 go1.14.2.linux-amd64.tar.gz | sha256sum -c -
tar xzf go1.14.2.linux-amd64.tar.gz -C /usr/local --strip-components=1 --no-same-owner
echo export PATH=$PATH:/usr/local/go/bin >> /etc/profile
rm go1.14.2.linux-amd64.tar.gz

##
## cleanup
##
apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false
rm -rf /var/lib/apt/lists/*