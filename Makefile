APPS=go node go-node nvm

all: $(APPS)

.clean:
	@rm -rf .build

$(APPS): .clean
	@mkdir .build
	@cp -r theia-base/* .build/
	@cp -r theia-$@/* .build/
	@docker build -t theia-$@ .build
	@rm -rf .build
